import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args){

        //Tworzenie gniazda, i sprawdzenie czy host/pory serwera nasłuchuje
        String host;
        int port;
        Scanner decision = new Scanner(System.in);
        String decision_String;
        host= "localhost";
        port = 998;
        Socket skt =null;
        try {
            //Próba połączenia z serwerem
            System.out.println("Klient: Próba podłączenia do serwera jako host-" + host + " port: " + (port+1) + '.');
            do{
                System.out.println("probuje");
                port++;
                skt = new Socket(host, port);
            }while (skt==null);
            //Opcje odczytu i zapisu z i do strumienia
            System.out.println("Popetli");
            Threads OutputThread = new Threads(false, skt);
            Threads InputThread = new Threads(true, skt);
            OutputThread.start();
            InputThread.start();


        }
           catch (IOException ex){
                ex.printStackTrace();
                System.out.println("Blad z buforami");
            }
    }
}