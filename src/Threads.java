import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Awdul on 30.03.2017.
 */
public class Threads extends Thread {
    private boolean whichThread;
    public BufferedReader Input;
    public PrintStream Output;
    Socket skt;
    String decision_String="1";
    Scanner decision = new Scanner(System.in);

    public Threads(boolean in, Socket _skt) {
        try {
            skt = _skt;
            Input = new BufferedReader(new InputStreamReader(skt.getInputStream())); //odczyt
            Output = new PrintStream(skt.getOutputStream());
        }
          catch (IOException ex){
            ex.printStackTrace();
            System.out.println("Blad z buforami");
        }
        if (in) {
            setWhichThread(true);

        } else {
            setWhichThread(false);
        }
    }

    public void run() {
        if (whichThread == false) //Output
        {
            try {
                //Sprawdzenie, czy serwer odpowiedział.
                for(String buf; (buf=Input.readLine())!=null;) {
                    if (buf != null) {
                        System.out.println(buf);
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println("Blad odczytu");
            }
        }
        else{
            try {
                while (decision_String.charAt(0) != 0) {
                    decision_String = decision.nextLine();
                    Output.println(decision_String);
                    Thread.sleep(100);
                }
                skt.close();
                System.out.println("Klient - Odłączony");
            }
            catch (IOException ex){
                ex.printStackTrace();
                System.out.println("Blad zapisu");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    void setWhichThread(boolean option) {
        if (option == true) whichThread = true;
        else whichThread = false;
    }

}
